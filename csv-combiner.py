#!/usr/bin/env python3

import sys
import csv
import os

def getFileList():
    files = list()
    
    print("Files must end in .csv")
    path = input("Please enter the path folder of csv files: ")
    path = path.strip() # Removes the trailing space from the path.
    
    if not os.path.isdir(path):
        print("The specified path is not a folder.  You failure.")
        sys.exit()
    
    for (dirpath, dirnames, filenames) in os.walk(path):
        files = files + [os.path.join(dirpath,file) for file in filenames if ".csv" in file]
    return (path, files)

def getData(files):
    data = list()
    fieldnames = list()
    
    for file in files:
        csvfile = csv.DictReader(open(file, 'rU'))
        if not fieldnames:
            fieldnames = csvfile.fieldnames
        
        if fieldnames != csvfile.fieldnames:
            print("Error: %s" % file)
            print('Header names must be the same for all files')
            sys.exit()
        print("Processing %s" % (file))
        data += [row for row in csvfile] # creates list of ordered dicts
    return (fieldnames, data)

if __name__ == '__main__':
    path, files = getFileList()
    fieldnames, data = getData(files)
    
    outfile = os.path.join(path, 'combined.csv')
    with open(outfile, 'w') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)
    